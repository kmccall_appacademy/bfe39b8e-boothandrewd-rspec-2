# lib/00_silly_blocks.rb

def reverser
  yield.split.map(&:reverse).join(' ')
end

def adder(delta = 1)
  yield + delta
end

def repeater(count = 1)
  count.times do
    yield
  end
end
