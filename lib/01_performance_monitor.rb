# lib/01_performance_monitor.rb
require 'time'

def measure(times_to_run = 1)
  times = (0...times_to_run).to_a.map do
    start_time = Time.now
    yield
    Time.now - start_time
  end
  times.reduce(:+).to_f / times.size
end
